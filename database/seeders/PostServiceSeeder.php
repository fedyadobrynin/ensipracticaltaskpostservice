<?php

namespace Database\Seeders;

use App\Domain\Posts\Models\Post;
use App\Domain\Votes\Models\Vote;
use Illuminate\Database\Seeder;

/**
 * Post service database seeder
 */
class PostServiceSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        Post::factory()->count(42)->has(Vote::factory()->count(24))->create();
    }
}
