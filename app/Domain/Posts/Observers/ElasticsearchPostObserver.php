<?php

namespace App\Domain\Posts\Observers;

use App\Domain\Posts\Actions\DeletePostInElasticAction;
use App\Domain\Posts\Actions\UpsertPostInElasticAction;
use App\Domain\Posts\Models\Post;

class ElasticsearchPostObserver
{
    public function __construct(
        protected DeletePostInElasticAction $deleteElasticAction,
        protected UpsertPostInElasticAction $upsertElasticAction,
    ) {
    }

    public function created(Post $post): void
    {
        $this->upsertElasticAction->execute($post);
    }

    public function updated(Post $post): void
    {
        $this->upsertElasticAction->execute($post);
    }

    public function deleted(Post $post): void
    {
        $this->deleteElasticAction->execute($post);
    }
}
