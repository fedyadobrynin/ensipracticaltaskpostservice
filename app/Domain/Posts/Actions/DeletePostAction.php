<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class DeletePostAction
{
    /**
     * @param int $postId
     * @return void
     */
    public function execute(int $postId): void
    {
        $targetPost = Post::findOrFail($postId);
        $targetPost->delete();
    }
}
