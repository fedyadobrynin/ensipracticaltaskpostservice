<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;

class DeletePostInElasticAction
{
    private Client $elcClient;

    public function __construct(Client $elcClient)
    {
        $this->elcClient = $elcClient;
    }


    public function execute(Post $post): void
    {
        $this->elcClient->delete([
            'index' => config('services.elasticsearch.host'),
            'id' => $post->id,
        ]);
    }
}
