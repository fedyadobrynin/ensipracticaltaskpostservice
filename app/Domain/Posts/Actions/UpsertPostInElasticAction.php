<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;

class UpsertPostInElasticAction
{
    private Client $elcClient;

    public function __construct(Client $elcClient)
    {
        $this->elcClient = $elcClient;
    }

    public function execute(Post $post): void
    {
        $this->elcClient->index([
            'index' => config('services.search.index'),
            'id' => $post->id,
            'body' => $post->toArray()
        ]);
    }
}
