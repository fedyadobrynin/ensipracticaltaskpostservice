<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class PatchPostAction
{
    /**
     * @param int $postId
     * @param array $fields
     * @return Post
     */
    public function execute(int $postId, array $fields): Post
    {
        $currentPost = Post::findOrFail($postId);
        $currentPost->update($fields);

        return $currentPost;
    }
}
