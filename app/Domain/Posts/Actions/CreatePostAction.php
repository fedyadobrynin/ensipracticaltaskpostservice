<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class CreatePostAction
{
    /**
     * @param array $fields
     * @return Post
     */
    public function execute(array $fields): Post
    {
        return Post::create($fields);
    }
}
