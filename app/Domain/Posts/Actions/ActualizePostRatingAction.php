<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class ActualizePostRatingAction
{
    /**
     * @param int $postId
     * @return void
     */
    public function execute(int $postId): void
    {
        $post = Post::findOrFail($postId);
        $post->rating = $post->votes()
            ->sum('vote');
        $post->save();
    }
}
