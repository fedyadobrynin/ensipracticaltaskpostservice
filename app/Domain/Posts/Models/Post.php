<?php

namespace App\Domain\Posts\Models;

use App\Domain\Posts\Models\Tests\PostFactory;
use App\Domain\Votes\Models\Vote;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'text',
        'user_id',
        'rating'
    ];

    /**
     * @return HasMany
     */
    public function votes(): HasMany
    {
        return $this->hasMany(Vote::class);
    }

    /**
     * @return PostFactory
     */
    public static function factory(): PostFactory
    {
        return PostFactory::new();
    }
}
