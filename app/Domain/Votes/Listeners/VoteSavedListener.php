<?php

namespace App\Domain\Votes\Listeners;

use App\Domain\Posts\Actions\ActualizePostRatingAction;
use App\Domain\Votes\Events\VoteSaved;

class VoteSavedListener
{
    /**
     * @param ActualizePostRatingAction $action
     */
    public function __construct(private readonly ActualizePostRatingAction $action)
    {
    }

    /**
     * Handle the event.
     *
     * @param VoteSaved $event
     * @return void
     */
    public function handle(VoteSaved $event): void
    {
        $postId = $event->vote->post_id;
        $this->action->execute($postId);
    }
}
