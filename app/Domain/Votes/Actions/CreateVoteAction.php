<?php

namespace App\Domain\Votes\Actions;

use App\Domain\Votes\Models\Vote;
use Exception;

class CreateVoteAction
{
    /**
     * @param array $fields
     * @return Vote
     * @throws Exception
     */
    public function execute(array $fields): Vote
    {
        $voteExists = Vote::where('user_id', $fields['user_id'])
            ->where('post_id', $fields['post_id'])
            ->exists();
        if ($voteExists) {
            throw new Exception('Голос пользователя уже был создан');
        }

        return Vote::create($fields);
    }
}
