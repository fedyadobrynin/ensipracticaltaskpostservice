<?php

namespace App\Domain\Votes\Actions;

use App\Domain\Votes\Models\Vote;

class DeleteVoteAction
{
    /**
     * @param int $voteId
     * @return void
     */
    public function execute(int $voteId): void
    {
        $targetVote = Vote::findOrFail($voteId);
        $targetVote->delete();
    }
}
