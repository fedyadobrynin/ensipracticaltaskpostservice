<?php

namespace App\Domain\Votes\Actions;

use App\Domain\Votes\Models\Vote;

class PatchVoteAction
{
    /**
     * @param int $voteId
     * @param array $fields
     * @return Vote
     */
    public function execute(int $voteId, array $fields): Vote
    {
        $currentVote = Vote::findOrFail($voteId);
        $currentVote->update($fields);

        return $currentVote;
    }
}
