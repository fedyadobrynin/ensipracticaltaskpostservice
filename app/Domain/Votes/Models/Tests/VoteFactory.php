<?php

namespace App\Domain\Votes\Models\Tests;

use App\Domain\Posts\Models\Post;
use App\Domain\Votes\Models\Vote;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Vote>
 */
class VoteFactory extends Factory
{
    protected $model = Vote::class;

    /**
     * Range of random values for user votes
     *
     * @var array|int[]
     */
    private array $randValue = [-1, 1];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 100),
            'post_id' => Post::factory(),
            'vote' => $this->faker->randomElement($this->randValue),
        ];
    }
}
