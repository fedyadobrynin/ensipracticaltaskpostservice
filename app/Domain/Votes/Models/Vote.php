<?php

namespace App\Domain\Votes\Models;

use App\Domain\Posts\Models\Post;
use App\Domain\Votes\Events\VoteDeleted;
use App\Domain\Votes\Events\VoteSaved;
use App\Domain\Votes\Models\Tests\VoteFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Vote extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'post_id',
        'vote'
    ];

    /**
     * @return VoteFactory
     */
    public static function factory(): VoteFactory
    {
        return VoteFactory::new();
    }

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => VoteSaved::class,
        'deleted' => VoteDeleted::class,
    ];
}
