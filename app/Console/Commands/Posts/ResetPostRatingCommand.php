<?php

namespace App\Console\Commands\Posts;

use App\Domain\Posts\Models\Post;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class ResetPostRatingCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected string $signature = 'posts:reset-rating {post?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected string $description = 'Обнуляет рейтинг заданного поста';

    /**
     * @return void
     * @throws Throwable
     */
    public function handle(): void
    {
        $postId = $this->argument('post');

        try {
            DB::transaction(function () use ($postId) {
                $post = Post::findOrFail($postId);
                $post->votes()->delete();
                $post->rating = 0;
                $post->save();
                $this->info('Рейтинг поста обнулен');
            });
        } catch (Exception $exception) {
            echo "Error deleting votes: ", $exception->getMessage(), PHP_EOL;
            throw $exception;
        }
    }
}
