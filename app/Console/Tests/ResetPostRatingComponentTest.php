<?php
namespace App\Console\Tests;

use App\Domain\Votes\Models\Vote;
use Tests\ComponentTestCase;
use App\Domain\Posts\Models\Post;
use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertDatabaseHas;

uses(ComponentTestCase::class);
uses()->group('component');

test("posts:reset-rating {post} success", function () {
    $posts = Post::factory()->count(1)->create();
    $resetPost = $posts->shift();
    Vote::factory()->for($resetPost)->count(10)->create();

    artisan("posts:reset-rating {$resetPost->id}");

    $posts->each(function($post) {
        assertDatabaseHas((new Post())->getTable(), [
            'id' => $post->id,
            'rating' => $post->rating
        ]);
    });
    assertDatabaseHas((new Post())->getTable(), [
        'id' => $resetPost->id,
        'rating' => 0
    ]);
    assertDatabaseMissing((new Vote())->getTable(), [
        'post_id' => $resetPost->id
    ]);
});
