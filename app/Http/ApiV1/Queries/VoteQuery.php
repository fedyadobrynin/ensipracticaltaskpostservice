<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Votes\Models\Vote;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class VoteQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Vote::query());

        $this->defaultSort('id');
        $this->allowedSorts([
            'id',
            'user_id',
            'post_id',
            'created_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('post_id'),
            AllowedFilter::exact('vote'),
            AllowedFilter::exact('created_at'),
        ]);
    }
}
