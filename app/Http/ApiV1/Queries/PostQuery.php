<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Posts\Models\Post;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PostQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Post::query());

        $this->allowedIncludes(
            [
                'vote',
            ]
        );

        $this->defaultSort('id');
        $this->allowedSorts([
            'id',
            'title',
            'rating',
            'created_at'
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('title'),
            AllowedFilter::exact('rating'),
            AllowedFilter::exact('user_id'),
        ]);
    }
}
