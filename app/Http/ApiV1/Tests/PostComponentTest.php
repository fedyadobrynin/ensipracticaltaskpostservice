<?php

use App\Domain\Posts\Models\Post;
use App\Domain\Votes\Models\Vote;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/posts/posts/{id} 200', function () {
    $newPost = Post::factory()::new([
        'title' => 'Test title',
        'text' => 'Test text',
        'user_id' => 1
    ])->has(Vote::factory()->count(10))->create();

    getJson('/api/v1/posts/posts/' . $newPost->id)
        ->assertStatus(200)
        ->assertJsonPath('data.title', $newPost['title'])
        ->assertJsonPath('data.text', $newPost['text'])
        ->assertJsonPath('data.user_id', $newPost['user_id']);

});

test('POST /api/v1/posts/posts 201 - success', function () {
    $request = Post::factory()::new([
        'title' => 'Test title',
        'text' => 'Test text',
        'user_id' => 1
    ])->make();

    $id = postJson('/api/v1/posts/posts', $request->toArray())
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas((new Post())->getTable(), [
        'id' => $id,
        'title' => $request['title'],
        'text' => $request['text'],
        'user_id' => $request['user_id']
    ]);
});

test('POST /api/v1/posts/posts:search filter 200', function () {
    Post::factory()->count(10)->create();

    $targetPost = Post::factory()->createOne([
        'title' => 'Test title',
        'text' => 'Test text',
        'user_id' => 1
    ]);
    $request = [
        'filter' => ['title' => 'Test title'],
    ];

    postJson('/api/v1/posts/posts:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $targetPost->id)
        ->assertJsonPath('data.0.title', $targetPost->title)
        ->assertJsonPath('data.0.text', $targetPost->text)
        ->assertJsonPath('data.0.user_id', $targetPost->user_id);
});

test('PATCH /api/v1/posts/posts/{id} 200', function () {
    $newPost = Post::factory()::new()->create();
    $request = Post::factory()::new()->make();

    patchJson("/api/v1/posts/posts/{$newPost->id}", $request->toArray())
        ->assertStatus(200)
        ->assertJsonPath('data.title', $request['title'])
        ->assertJsonPath('data.text', $request['text'])
        ->assertJsonPath('data.user_id', $request['user_id']);
});

test('DELETE /api/v1/posts/posts/{id} 200 - success', function () {
    $post = Post::factory()::new()->has(Vote::factory()->count(10))->create();

    deleteJson('/api/v1/posts/posts/' . $post->id)
        ->assertStatus(200);

    assertDatabaseMissing((new Post())->getTable(), [
        'id' => $post->id,
    ]);
    assertDatabaseMissing((new Vote())->getTable(), [
        'post_id' => $post->id,
    ]);
});

