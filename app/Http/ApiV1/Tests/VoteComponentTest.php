<?php

use App\Domain\Votes\Models\Vote;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/posts/votes 201', function () {
    $request = Vote::factory()::new([
        'user_id' => 42,
        'vote' => 1
    ])->make();

    $id = postJson('/api/v1/posts/votes', $request->toArray())
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas((new Vote())->getTable(), [
        'id' => $id,
        'user_id' => $request['user_id'],
        'vote' => $request['vote']
    ]);
});

test('POST /api/v1/posts/votes:search filter 200', function () {
    Vote::factory()->count(10)->create();

    $targetVote = Vote::factory()->createOne([
        'user_id' => 42,
        'vote' => 1
    ]);
    $request = [
        'filter' => ['user_id' => 42],
    ];

    postJson('/api/v1/posts/posts:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $targetVote->id)
        ->assertJsonPath('data.0.user_id', $targetVote->user_id)
        ->assertJsonPath('data.0.vote', $targetVote->vote);
});

test('PATCH /api/v1/posts/votes/{id} 200', function () {
    $newVoice = Vote::factory()::new()->create();
    $request = Vote::factory()::new()->make();

    patchJson('/api/v1/posts/votes/' . $newVoice->id, $request->toArray())
        ->assertStatus(200)
        ->assertJsonPath('data.vote', $request['vote'])
        ->assertJsonPath('data.user_id', $request['user_id'])
        ->assertJsonPath('data.user_id', $request['post_id']);

});

test('DELETE /api/v1/posts/votes/{id} 200', function () {
    $vote = Vote::factory()::new()->create();

    deleteJson('/api/v1/posts/votes/' . $vote->id)
        ->assertStatus(200);

    assertDatabaseMissing((new Vote())->getTable(), [
        'id' => $vote->id,
    ]);
});

