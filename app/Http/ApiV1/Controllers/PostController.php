<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Posts\Actions\PatchPostAction;
use App\Domain\Posts\Actions\CreatePostAction;
use App\Domain\Posts\Actions\DeletePostAction;
use App\Http\ApiV1\Queries\PostQuery;
use App\Http\ApiV1\Requests\CreatePostRequest;
use App\Http\ApiV1\Requests\PatchPostRequest;
use App\Http\ApiV1\Resources\PostResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PostController
{
    /**
     * @param CreatePostRequest $request
     * @param CreatePostAction $action
     * @return PostResource
     */
    public function create(CreatePostRequest $request, CreatePostAction $action): PostResource
    {
        return new PostResource($action->execute($request->validated()));
    }

    /**
     * @param int $postId
     * @param PostQuery $query
     * @return PostResource
     */
    public function get(int $postId, PostQuery $query): PostResource
    {
        $result = $query->findOrFail($postId);

        return new PostResource($result);
    }

    /**
     * @param int $postId
     * @param DeletePostAction $action
     * @return EmptyResource
     */
    public function delete(int $postId, DeletePostAction $action): EmptyResource
    {
        $action->execute($postId);

        return new EmptyResource();
    }

    /**
     * @param int $postId
     * @param PatchPostRequest $request
     * @param PatchPostAction $action
     * @return PostResource
     */
    public function patch(int $postId, PatchPostRequest $request,  PatchPostAction $action): PostResource
    {
        return new PostResource($action->execute($postId, $request->validated()));
    }

    /**
     * @param PageBuilderFactory $pageBuilderFactory
     * @param PostQuery $query
     * @return AnonymousResourceCollection
     */
    public function searchPosts(PageBuilderFactory $pageBuilderFactory, PostQuery $query): AnonymousResourceCollection
    {
        return PostResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

}
