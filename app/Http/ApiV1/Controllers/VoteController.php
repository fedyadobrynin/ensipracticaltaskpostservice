<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Votes\Actions\CreateVoteAction;
use App\Domain\Votes\Actions\DeleteVoteAction;
use App\Domain\Votes\Actions\PatchVoteAction;
use App\Http\ApiV1\Queries\VoteQuery;
use App\Http\ApiV1\Requests\CreateVoteRequest;
use App\Http\ApiV1\Requests\PatchVoteRequest;
use App\Http\ApiV1\Resources\VoteResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VoteController
{
    /**
     * @param CreateVoteRequest $request
     * @param CreateVoteAction $action
     * @return VoteResource
     */
    public function create(CreateVoteRequest $request, CreateVoteAction $action): VoteResource
    {
        return new VoteResource($action->execute($request->validated()));
    }

    /**
     * @param int $voteId
     * @param DeleteVoteAction $action
     * @return EmptyResource
     * @throws \Throwable
     */
    public function delete(int $voteId, DeleteVoteAction $action): EmptyResource
    {
        $action->execute($voteId);

        return new EmptyResource();
    }

    /**
     * @param int $voteId
     * @param PatchVoteRequest $request
     * @param PatchVoteAction $action
     * @return VoteResource
     */
    public function patch(int $voteId, PatchVoteRequest $request, PatchVoteAction $action): VoteResource
    {
        return new VoteResource($action->execute($voteId, $request->validated()));
    }

    /**
     * @param PageBuilderFactory $pageBuilderFactory
     * @param VoteQuery $query
     * @return AnonymousResourceCollection
     */
    public function searchVote(PageBuilderFactory $pageBuilderFactory, VoteQuery $query): AnonymousResourceCollection
    {
        return VoteResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
